HISUserAdmin.controller('administracionUsuariosController', ['$compile','$cookieStore','$scope','Session','erroresServices','mensajesData', function($compile,$cookieStore,$scope,Session,erroresServices,mensajesData){

     
      $scope.formData = {
        
        //  "confirmacionCreacion": 0
      };

      $scope.reanudarPantalla = function () {
        location.reload();
      
      }

      $scope.cierreDeSesion = function () {

        $cookieStore.remove('tokenActual');
         location.reload();
         window.location= "/#/";
      }

      $scope.submitForm = function (formData,TipodeGrabacion,isValid) {

          // agregar usuario 
          
        $scope.submitted = false;
        if(isValid){

          if(TipodeGrabacion==0 || TipodeGrabacion==1){


                if(TipodeGrabacion==1){
                  $scope.formData.confirmacionCreacion = 1;        
                };

                $scope.datos = JSON.stringify(formData);
               
                
                Session.UAUsers().save($scope.datos, function(savedRecord) {
                    mensajesData.mensajeSave(function(cb){
                        $('#popupNuevoUsuario').modal('hide');

                    });
 
                     $scope.regs = Session.UAUsers().query(function(result) {
                      $scope.registrations.read();
                   });
                      

                },function(error){
                   
                }); 
            }

          if(TipodeGrabacion==2){
                Session.ReactivarUsuario($cookieStore.get('tokenActual'),$scope.idUsuario).then(function(responseMessage){
                $('#popupNuevoUsuario').modal('hide');
                $('#popupUsuarioExistente').modal('hide');
                $scope.errorTittle ="Reactivación de usuario";
                $scope.errorMensaje ="Se reactivo el usuario";
                $scope.errorFuncion= "reanudarPantalla";
                $('#popupErrores').modal('show'); 
                
                },function(error){
           
             
                });
          }
        }
        else
        {
          $scope.submitted = true;
        }
      };


      $scope.EliminarUsuario = function () {

          
          Session.UAUsers().delete({id: $scope.eliminarIdUsuario},function(responseMessage) { 
                
                mensajesData.mensajeDelete(function(cb){
                    $('#popupEliminarUsuario').modal('hide');
                });
              
             //  $scope.registrations();
             $scope.regs = Session.UAUsers().query(function(result) {
                  $scope.registrations.read();
            });

          },function(error){
               var status = error.status;
               if(status==401){
                };

                if(status==500){     
                };
            }); 
      };

      $scope.ListarRoles = function (llegada) {

       $scope.roles= Session.UARoles().get(function(responseMessage) { 
                    },function(error){

                    });
       $scope.formData={}
       $scope.formData.rol="1"
       

      };


      var cantidadPaginacion= 10;
      var inicioPaginacion= 0;

      $scope.regs;

      $scope.registrations = new kendo.data.DataSource({
      transport: {
        read: function(options) {
             options.success($scope.regs);
        }
    },
  
     schema: {
            /*   data: "listaUsuarios",
                type: 'json', 
                total: 'total',
              */  model: {

                      fields: {
                          id: { type: "string" }, 
                          nombres: { type: "string" },
                          apellidos: { type: "string" },
                          usuario: { type: "string" },
                          telefonoContacto: { type: "string" },
                          DNI: { type: "string" },
                          estado: { type: "string" }
                      }
                }
              },
    pageSize: 10,
    serverPaging: true,
    serverFiltering: true,
    serverSorting: true
});


$scope.registrationsColumns = [       
                        {field:"nombres",
                          title:"Nombre",
                          filtereable: false},

                        {field:"apellidos",
                          title:"Apellidos",
                          filtereable: false},

                        {field:"correo",
                          title:"Correo",
                          filtereable: false},

                        {field:"estado",
                          title:"Estado",
                          filtereable: false},

                        {command:  [{ text: " ", template:'<kendo-button sprite-css-class="\'k-icon k-i-pencil\'" ng-click="mostrarEdicionUsuarios(dataItem)"></kendo-button>'}
                              , { text: " ", template: '<kendo-button sprite-css-class="\'k-icon k-i-close\'"  ng-click="eliminacionDeUsuarios(dataItem)" ></kendo-button>'}]}
                         
          ];

      $scope.gridOptions = {
             
    
          height: 550,
          filterable: false,
          sortable: true,
          pageable: true,
          
      };

  $scope.regs = Session.UAUsers().query(function(result) {
        $scope.registrations.read();
  });

     
      $scope.eliminacionDeUsuarios = function(dataItem){
          $scope.eliminarIdUsuario=dataItem.id;
          $('#popupEliminarUsuario').modal('show');

         
      }

      $scope.mostrarEdicionUsuarios = function(dataItem){
          
          $scope.ListarRoles('editar');
          $scope.tempDataItem = dataItem;
          $scope.editData ={
                _id: dataItem.id,
                codigo: dataItem.usuario,
                nombres: dataItem.nombres,
                apellidos: dataItem.apellidos,
                correo: dataItem.correo,
                telefonoContacto: dataItem.telefono,
                DNI: parseInt(dataItem.dni),
                rol:(dataItem.rol).toString(),
                estado:dataItem.estado
          };
                 
         
          $scope.submitted = false;

          $('#popupEditarUsuario').modal('show');
      
      };

       $scope.editarUsuario= function(data,isValid){

        $scope.submitted = false;
        if(isValid){
              
                 Session.UAUsers().update($scope.editData, function(savedRecord) {

                  $scope.tempDataItem.nombres = $scope.editData.nombres;
                  $scope.regs = Session.UAUsers().query(function(result) {
                    $scope.registrations.read();
                  });
                  $('#popupEditarUsuario').modal('hide');
                  $scope.submitted = false;
                  mensajesData.mensajeUpdate();

                 },function (error){
                     erroresServices.controlError(error.status);

                 });
        }
        else
        {
          $scope.submitted = true;
        }
      };

      $scope.desbloquearUsuario= function()
      {
        Session.desbloqueoUsuario().get({id:$scope.editData._id},function(responseMessage) { 
           
                      $scope.regs = Session.UAUsers().query(function(result) {
                        $scope.registrations.read();
                      });
                    },function(error){

                    });
      }

}])