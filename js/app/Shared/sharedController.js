HISShared.controller('menuController',['$scope','$filter','$rootScope','menuService','ProfileService','$cookieStore', function($scope,$filter,$rootScope,menuService,ProfileService,$cookieStore){
    menuService.listarMenu().query(function(result) { 
        $scope.modulos = [];
       
                   for (i = 0; i < result.length; i++){
                    var agregado = false;
                    for (j=0; j< $scope.modulos.length; j++){
                      if (result[i].modulo === $scope.modulos[j].nombre){
                        $scope.modulos[j].funcionalidades.push(result[i]);
                        agregado = true;
                      }
                      if (agregado)
                        break;
                    }
                    if (agregado == false){
                      var itemModulo = {};
                      itemModulo.nombre = result[i].modulo;
                      itemModulo.funcionalidades = [];
                      itemModulo.funcionalidades.push(result[i]);
                      $scope.modulos.push(itemModulo);
                    }

                   }
            
      })


    $scope.cerrarSesion =function(){


        $cookieStore.remove('tokenActual');
       

         ProfileService.Perfil().delete({}, function() {
           
          localStorage.removeItem('sJWT');
          window.location= "/#/";
          
        },function (error){
          console.log(error);
            localStorage.removeItem('sJWT');
             localStorage.removeItem('funcionalidades');
          window.location= "/#/";
            erroresServices.controlError(error.status);

        });
         


    } ;





}]),
HISShared.controller('directivaMostrarAgregarDatosController',['$scope','$filter','$rootScope','directivaMostrarAgregarDatosServices',function($scope,$filter,$rootScope,directivaMostrarAgregarDatosServices){
          
          $scope.existe=true;

          $scope.datosContactos={}
          $scope.cargarDatos=function(){
            
            $scope.datosContactos= directivaMostrarAgregarDatosServices.directivaAgregar($scope.modelo.modelo).query($scope.modelo.filtro,function(result) {  
                
            });
          }

          $scope.validarExistencia=function() {
              $scope.existe=false;
         
              for(i=0; i<$scope.datosContactos.length;i++)
              {    
                   
                    if($scope.datosMostrarContacto[$scope.modelo.placeholder] == $scope.datosContactos[i].nombre){
                        $scope.existe=true;
                        break;
                    };
              };
              
                            
          }

          $scope.crearNuevoValor=function(){
              $scope.datosGuardar={
                nombre:$scope.datosMostrarContacto[$scope.modelo.placeholder],
                tipo:$scope.modelo.placeholder
              };
              directivaMostrarAgregarDatosServices.directivaAgregar($scope.modelo.modelo).save($scope.datosGuardar, function() {
                    $scope.existe=true;
                    $scope.cargarDatos();
              }); 
          }
          $scope.cargarDatos();
     
}]),
HISShared.controller('directivaFcaturasVencidas',['$scope','$rootScope','facturasVencidasService',function($scope,$rootScope,facturasVencidasService){
  
  $scope.ini=function(){
      $scope.regs;
      $scope.mostrarFiltros = false;
      
      $scope.registrationsFactVencidas = new kendo.data.DataSource({

        transport: {
            read: function(options) {
                 options.success($scope.regsFactVencidas);
            }
        },
        schema: {
              model: {
                  fields: {
                      num_factura: { type: "string" ,editable: false},
                      nombre_cliente: { type: "string" ,editable: false},                  
                      fecha_vencimiento: { type: "date" ,editable: false},
                      total_cotizacion: { type: "number" ,editable: false}
                  }
              }
        },
        pageSize: 10,
        Paging: true
      });

      $scope.registrationsColumnsFactVencidas = [
                            {
                              field:"num_factura",
                              title:"Número"
                            },
                            {
                              field:"nombre_cliente",
                              title:"Cliente"
                            },
                           
                            {
                              field:"fecha_vencimiento",
                              title:"Fecha de Vencimiento",
                              type: "date",
                              format:"{0:dd/MM/yyyy}"
                            },
                            {
                              field:"total_factura",
                              title:"Total"
                             },
                            {
                              command:  [{ text: " ", template: '<kendo-button sprite-css-class="\'k-icon k-i-pencil\'" ng-click="mostrarFacturaVencidas(dataItem)"></kendo-button>'}]}
      ];

      $scope.gridOptionsFactVencidas = {
          height: 570,
          filterable: {
                                mode: "row",
                                operators: {
                                    date: {
                                        eq: "Igual a",
                                        neq: "Diferente a",
                                        gte: "Mayor o igual a",
                                        gt: "Mayor a",  
                                        lte: "Menor o igual a",
                                        lt: "Menor a"
                                    },
                                    number: {
                                        eq: "Igual a",
                                        neq: "Diferente a",
                                        gte: "Mayor o igual a",
                                        gt: "Mayor a",  
                                        lte: "Menor o igual a",
                                        lt: "Menor a"                                    
                                    },
                                    string: {
                                        startswith: "Inicia con",
                                        endswith: "Termina con",
                                        eq: "Es igual a",
                                        neq: "Es diferente a",
                                        contains:"Contiene",
                                        doesnotcontain:"No Contiene"
                                    }
                                   
                                },
                                messages:{
                                  and:"Y",
                                  or:"O",
                                  filter:"Filtrar",
                                  clear:"Limpiar",
                                  info:"Mostrar los valores que sean"
                                }
                        },    
              sortable: true,
              reorderable: true,
              resizable: true,   
               editable: true, 
               columnMenu: {
                  messages:{
                    sortAscending:"Ordenar Ascendentemente",
                    sortDescending:"Ordenar Descendentemente",
                    columns:"Columnas",
                    filter:"Filtro Especial",
                  }
               } ,        
              pageable: {
                  messages: {
                      display: "{0} - {1} de {2} Registros", 
                      empty: "No existen datos",
                      page: "Página",
                      of: "de {0}",
                      itemsPerPage: "Páginas",
                      first: "Primero",
                      previous: "Anterior",
                      next: "Siguiente",
                      last: "Último",
                      refresh: "Refrescar"
                  }
              }
      }

      $scope.regsFactVencidas = facturasVencidasService.factVencidas().get(function(result) {
          $("#gridFacturasVencidas .k-filter-row").hide();
          $scope.registrationsFactVencidas.read();     
          
      },function (error){

      });
  }

  $scope.mostrarFacturaVencidas = function(dataItem){
    $scope.formGrabar ={
      id:dataItem._id
    };
    window.location= "#/facturas/"+dataItem.id;
  };
  
  $scope.ini()

  $scope.mostrarOcultarFiltrosFactVenc = function(){
    if($scope.mostrarFiltros){
      $("#gridFacturasVencidas .k-filter-row").hide();
      $scope.mostrarFiltros = false;
    }else{
      $("#gridFacturasVencidas .k-filter-row").show();
      $scope.mostrarFiltros = true;
    }
  }



}]),