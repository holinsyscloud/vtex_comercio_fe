HISShared.directive('popupMensaje', function() {
    return {
     restrict : 'E',
     templateUrl : 'js/app/Shared/popUpMensaje.html'

    };
}),
HISShared.directive('menuInicio', function() {
    return {
     restrict : 'E',
     templateUrl : 'js/app/Shared/menu.html'

    };
}),
HISShared.directive('mensajeSaveData', function() {
    return {
     restrict : 'E',
     templateUrl : 'js/app/Shared/popUpMensajeSave.html'

    };
}),
HISShared.directive('mensajeDeleteData', function() {
    return {
     restrict : 'E',
     templateUrl : 'js/app/Shared/popUpMensajeDelete.html'

    };
}),

HISShared.directive('mensajeUpdateData', function() {
    return {
     restrict : 'E',
     templateUrl : 'js/app/Shared/popUpMensajeUpdate.html'

    };
}),
HISShared.directive('mensajeErrorCheque', function() {
    return {
     restrict : 'E',
     templateUrl : 'js/app/Shared/popUpMensajeErrorCheque.html'

    };
}),
HISShared.directive('mensajesErroresGlobales', function() {
    return {
     restrict : 'E',
     templateUrl : 'js/app/Shared/popUps/popUpErroresGlobales.html'

    };
}),

HISShared.directive('facturasPendientes', function() {
    return {
     restrict : 'E',
     templateUrl : 'js/app/Facturacion/facturasVencidasView.html',
     controller:'directivaFcaturasVencidas'

    };
}),


HISShared.directive("loadingIndicator", function (loadingCounts, $timeout) {
    return {
        restrict: "A",
        link: function (scope, element, attrs) {
            scope.$on("loading-started", function (e) {
                loadingCounts.enable_count++;
                console.log("displaying indicator " + loadingCounts.enable_count);
                //only show if longer than one sencond
                $timeout(function () {
                    if (loadingCounts.enable_count > loadingCounts.disable_count) {
                        element.css({ "display": "" });
                    }
                }, 10);
            });
            scope.$on("loading-complete", function (e) {
                loadingCounts.disable_count++;
                console.log("hiding indicator " + loadingCounts.disable_count);
                if (loadingCounts.enable_count == loadingCounts.disable_count) {
                    element.css({ "display": "none" });
                }
            });
        }
    };
});



HISShared.directive('mostrarAgregarDatos', function() {
    
	template = 

         '<div ng-controller="directivaMostrarAgregarDatosController">'+ 
         
					'<input class="form-control" type="text" ng-change="validarExistencia()" name="dpd{{modelo.placeholder}}" list="{{modelo.placeholder}}" placeholder="Ingrese un {{modelo.placeholder}}" ng-model="datosMostrarContacto[modelo.placeholder]" required/>'+
					'<datalist  id="{{modelo.placeholder}}">'+
					      '<option ng-repeat="datosContactos in datosContactos"> {{datosContactos.nombre}} </option>'+
				        
					'</datalist>'+
      //    '<label ng-click="crearNuevoValor()" ng-if="!existe && datosMostrarContacto.contacto != null" style="cursor: pointer;">Agregar {{datosMostrarContacto.placeholder}}</label>'+
          '<label ng-click="crearNuevoValor()" ng-if="!existe " style="cursor: pointer;">Agregar {{datosMostrarContacto.placeholder}}</label>'+
                  '</div>'

 
    return {
     restrict : 'EA',
     scope: {
              
              datosMostrarContacto:'=dato',
              modelo:'=modelo'

          },
     
     template: template
     

    };
})
