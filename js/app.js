var app = angular.module('HIS', ['ngRoute',

 
  'kendo.directives' , 
  'ngCookies',
  'ngResource',
  'globalErrors',
  'ui.bootstrap',
  'HIS.Shared',
  'HIS.Profile',  
  'HIS.UserAdmin'
  
  ])

app.run(['$rootScope','$cookieStore','ProfileService',  function($rootScope, $cookieStore,ProfileService) {
 

   if (localStorage.getItem("sJWT") === null){

        if ($cookieStore.get('tokenActual') === null  || !$cookieStore.get('tokenActual')  ){
                window.location= "index.html";
        }else{
             localStorage.setItem('sJWT', $cookieStore.get('tokenActual')); 

        }

   }else
       if (localStorage.getItem("funcionalidades") === null){  
              ProfileService.Parametros().get(function(objParametros) {
               
                  for(i=0;i<objParametros.length;i++)
                  {
                    localStorage.setItem(objParametros[i].nombreParam,JSON.stringify(objParametros[i].valorParam) ); 
                  }

              })

   
        }
  
}]);

app.config(['$routeProvider',function($routeProvider) {

 

 EvaluarAcceso=  function(valor){

    if (localStorage.getItem("funcionalidades") != null){
      var funcionPermitida=false;
      var paramFuncionalidades= JSON.parse(localStorage.getItem('funcionalidades'));

      for(i=0; i<paramFuncionalidades.length;i++)
      {
        if(paramFuncionalidades[i] == valor)
        {
          funcionPermitida=true;
          break;
        }
      }

      if(!funcionPermitida){
        alert('olakeace');
        window.location= "/";
      }
   }
  };

 
    $routeProvider
        .when('/uploadsof', {
            templateUrl : 'js/app/Shared/upload.html',
             controller  : 'CUClienteController'
         })


         .when('/usuario/gruposUsuarios', {
            templateUrl : 'js/app/GroupUser/groupUserView.html',
            controller  : 'GroupUserController',
            resolve:{
                  "check": function(){ EvaluarAcceso('Grupo de usuarios')}
                }
         })
          .when('/usuario/gruposUsuarios/nuevoGrupo', {
            templateUrl : 'js/app/GroupUser/groupUserCUView.html',
            controller  : 'NewGroupUserController',
            resolve:{
                  "check": function(){ EvaluarAcceso('Grupo de usuarios')}
                }
         })
          .when('/usuario/gruposUsuarios/Grupo/:idGrupo', {
            templateUrl : 'js/app/GroupUser/groupUserCUView.html',
            controller  : 'NewGroupUserController',
            resolve:{
                  "check": function(){ EvaluarAcceso('Grupo de usuarios')}
                }
         })
          .when('/cotizaciones', {
            templateUrl : 'js/app/Cotizacion/cotizacionView.html',
            controller  : 'CotizacionController',
            resolve:{
                  "check": function(){ EvaluarAcceso('Cotizaciones')}
                }
         })

         .when('/cotizaciones/detalle/:idCotizacion', {
            templateUrl : 'js/app/Cotizacion/detalleCotizacionView.html',
            controller  : 'cotizacionesDetalleController',
             resolve:{
                 "check": function(){ EvaluarAcceso('Cotizaciones')} 
              }
         })

         .when('/cotizaciones/nuevo', {
            templateUrl : 'js/app/Cotizacion/detalleCotizacionView.html',
            controller  : 'cotizacionesDetalleController',
             resolve:{
                 "check": function(){ EvaluarAcceso('Cotizaciones')} 
              }
         })

         .when('/cotizaciones/textoCotizaciones', {
            templateUrl : 'js/app/Cotizacion/textoCotizacionView.html',
            controller  : 'textoCotizacionController',
             resolve:{
                 "check": function(){ EvaluarAcceso('Cotizaciones')} 
                
              }
           
         })
         .when('/cotizaciones/textoCotizaciones/nuevoTexto', {
            templateUrl : 'js/app/Cotizacion/textoCotizacionCUView.html',
            controller  : 'CUTextoCotizacionController',
            resolve:{
                  "check": function(){ EvaluarAcceso('Cotizaciones')}
                }
         })
         .when('/cotizaciones/textoCotizaciones/:idGrupo', {
            templateUrl : 'js/app/Cotizacion/textoCotizacionCUView.html',
            controller  : 'CUTextoCotizacionController',
            resolve:{
                  "check": function(){ EvaluarAcceso('Cotizaciones')}
                }
         })

          .when('/facturas', {
            templateUrl : 'js/app/Facturacion/facturas.html',
            controller  : 'FacturasController',
            resolve:{
                  "check": function(){ EvaluarAcceso('Facturas')}
                }
         })
         .when('/facturas/nuevo', {
            templateUrl : 'js/app/Facturacion/detalleFactura.html',
            controller  : 'detalleFacturaController',
             resolve:{
                 "check": function(){ EvaluarAcceso('Facturas')}
              }
         })
         .when('/facturas/:idFactura', {
            templateUrl : 'js/app/Facturacion/detalleFactura.html',
            controller  : 'detalleFacturaController',
             resolve:{
                 "check": function(){ EvaluarAcceso('Facturas')}
              }
         })
          .when('/generarFacturas', {
            templateUrl : 'js/app/Facturacion/generacionFacturas.html',
            controller  : 'generacionFacturasController',
           /*  resolve:{
                 "check": function(){ EvaluarAcceso('Facturas')}
              }*/
         })
          .when('/clientes', {
            templateUrl : 'js/app/Clientes/clientesView.html',
            controller  : 'ClientesController',
            resolve:{
                  "check": function(){ EvaluarAcceso('Clientes')}
                }
         })
          .when('/clientes/nuevo', {
            templateUrl : 'js/app/Clientes/clientesCUView.html',
            controller  : 'CUClienteController',
            resolve:{
                  "check": function(){ EvaluarAcceso('Clientes')}
                }
         })
          .when('/clientes/:idCliente', {
            templateUrl : 'js/app/Clientes/clientesCUView.html',
            controller  : 'CUClienteController',
            resolve:{
                  "check": function(){ EvaluarAcceso('Clientes')}
                }
         })
         

         .when('/articulos', {
            templateUrl : 'js/app/Articulos/articulosView.html',
            controller  : 'ArticulosController',
            resolve:{
                  "check": function(){ EvaluarAcceso('Articulos')}
                }
         })
          .when('/articulos/nuevo', {
            templateUrl : 'js/app/Articulos/articulosCUView.html',
            controller  : 'CUArticuloController',
            resolve:{
                  "check": function(){ EvaluarAcceso('Articulos')}
                }
         })
          .when('/articulos/:idArticulo', {
            templateUrl : 'js/app/Articulos/articulosCUView.html',
            controller  : 'CUArticuloController',
            resolve:{
                  "check": function(){ EvaluarAcceso('Articulos')}
                }
         })

         .when('/centrocostos', {
            templateUrl : 'js/app/CentroCosto/centrocostosView.html',
            controller  : 'CentroCostosController',
            resolve:{
                  "check": function(){ EvaluarAcceso('CentroCostos')}
                }
         })
          .when('/centrocostos/nuevo', {
            templateUrl : 'js/app/CentroCosto/centrocostosCUView.html',
            controller  : 'CUCentroCostoController',
            resolve:{
                  "check": function(){ EvaluarAcceso('CentroCostos')}
                }
         })
          .when('/centrocostos/:idCentroCosto', {
            templateUrl : 'js/app/CentroCosto/centrocostosCUView.html',
            controller  : 'CUCentroCostoController',
            resolve:{
                  "check": function(){ EvaluarAcceso('CentroCostos')}
                }
         })
        .when('/cuentaContable', {
            templateUrl : 'js/app/CuentaContable/cuentaContableView.html',
            controller  : 'CuentaContableController',
            resolve:{
                  "check": function(){ EvaluarAcceso('CuentaContable')}
                }
         })
          .when('/cuentaContable/nuevo', {
            templateUrl : 'js/app/CuentaContable/cuentaContableCUView.html',
            controller  : 'CUCuentaContableController',
            resolve:{
                  "check": function(){ EvaluarAcceso('CuentaContable')}
                }
         })
          .when('/cuentaContable/:idCuentaContable', {
            templateUrl : 'js/app/CuentaContable/cuentaContableCUView.html',
            controller  : 'CUCuentaContableController',
            resolve:{
                  "check": function(){ EvaluarAcceso('CuentaContable')}
                }
         })

        .when('/user/admin', {
            templateUrl : 'js/app/UserAdmin/administracionUsuarioView.html',
            controller  : 'administracionUsuariosController',
            resolve:{
                  "check": function(){ EvaluarAcceso('UserAdmin')}
                }
         })  
        .when('/cambiarContrasenia', {
            templateUrl : 'js/app/Profile/cambiarPasswordView.html',
            controller  : 'ProfileCambiarPassController',
            resolve:{
                 "check": function(){ EvaluarAcceso('Cambiar Contraseña')}  
                 
              }
           
         })

       .otherwise({
            redirectTo: '/'
        });
    
    
}])